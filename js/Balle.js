/**
 * Objet permettant de définir la balle.
 */
class Balle {
    /**
     * Constructeur.
     */
    constructor() {
        this.m_rayon = 6;
        this.m_x = 0;
        this.m_y = 0;
    }

    /**
     * Permet de rafraichir la position de la balle.
     * @param p_x est la nouvelle position sur l'axe horizontal.
     * @param p_y est la nouvelle position sur l'axe vertical.
     */
    rafraichirPosition(p_x, p_y) {
        this.m_x = p_x;
        this.m_y = p_y;
    }

    /**
     * Permet de dessiner la balle.
     * @param {Crayon} p_crayon est le crayon permettant de dessiner.
     */
    dessiner(p_crayon) {
        // On dessine la balle.
        // TODO :  La balle est un cercle qu'il faut dessiner avec le crayon.
    }

    /**
     * Retourne la position en x.
     * @returns Retourne la position en x.
     */
    getX() {
        return this.m_x;
    }

    /**
     * Retourne la position en y.
     * @returns Retourne la position en y.
     */
    getY() {
        return this.m_y;
    }

    /**
     * Retourne le rayon.
     * @returns Retourne le rayon.
     */
    getRayon() {
        return this.m_rayon;
    }
}
