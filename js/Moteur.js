/**
 * Permet de définir le moteur du jeu. Gestion des données et du dessin.
 */
class Moteur {
    /**
     * Constructeur.
     * @param p_canvas est le canvas de la page html.
     */
    constructor(p_canvas) {
        // Largeur et hauteur du terrain.
        this.m_largeurTerrain = 400;
        this.m_hauteurTerrain = 600;

        // Canvas de l'html.
        this.m_canvas = p_canvas;

        // On retaille le canvas sur la taille du terrain.
        this.m_canvas.width = this.m_largeurTerrain;
        this.m_canvas.height = this.m_hauteurTerrain;

        // Crayon permettant de dessiner.
        this.m_crayon = new Crayon(this.m_canvas.getContext('2d'));

        // Gestion du clavier.
        this.m_clavier = new Clavier();

        // Démarre l'écoute du clavier.
        this.m_clavier.ajouterEcouteClavier();

        // Création du terrain.
        this.m_terrain = new Terrain(this.m_largeurTerrain, this.m_hauteurTerrain);

        // Création de la balle.
        this.m_balle = new Balle();

        // Création de la raquette de la joueuse.
        this.m_raquetteJoueuse = new Raquette();

        // Création de la raquette de l'ordinateur.
        this.m_raquetteOrdinateur = new Raquette();

        // Création comportement de la raquette de la joueuse .
        this.m_comportementRaquetteJoueuse = new ComportementRaquetteJoueuse(this.m_raquetteJoueuse, this.m_terrain, this.m_balle, this.m_clavier);

        // Création comportement de la raquette de l'ordinateur.
        this.m_comportementRaquetteOrdinateur = new ComportementRaquetteOrdinateur(this.m_raquetteOrdinateur, this.m_terrain, this.m_balle);

        // Création comportement de la balle.
        this.m_comportementBalle = new ComportementBalle(this.m_balle, this.m_raquetteJoueuse, this.m_raquetteOrdinateur, this.m_terrain);

        // Gestion du score.
        this.m_scoreJoueuse = 0;
        this.m_scoreOrdinateur = 0;

        // Commencer échange.
        this.commencerEchange();
    }

    /**
     * Permet de rafrachir les données du moteur.
     */
    rafraichir() {
        // Test si l'ordinateur rate la balle : la joueuse gagne l'échange.
        if (this.m_comportementRaquetteOrdinateur.rateBalle() == true) {
            this.joueuseGagneEchange();
        }
        // Test si la joueuse rate la balle : l'ordinateur gagne l'échange.
        else if (this.m_comportementRaquetteJoueuse.rateBalle() == true) {
            this.ordinateurGagneEchange();
        }
        // Sinon on laisse le jeu se poursuivre.
        else {
            // On rafraichit la position de la raquette de la joueuse.
            this.m_comportementRaquetteJoueuse.rafraichirRaquetteJoueuse();

            // On rafrachit la position de le raquette de l'ordinateur.
            this.m_comportementRaquetteOrdinateur.rafraichirRaquetteOrdinateur();

            // On rafraichit la position de la balle.
            this.m_comportementBalle.rafraichirBalle();

            // Gestion collision balle avec raquette.
            this.gestionCollisionBalleAvecRaquette();
        }
    }

    /**
     * Permet de gérer la collision de la balle avec les raquette.
     */
    gestionCollisionBalleAvecRaquette() {
        // Test s'il y a une collision avec la raquette de l'ordinateur.
        if (this.m_comportementRaquetteOrdinateur.testCollisionAvecBalle() == true) {
            // On change les directions à cause de l'impact de la raquette.
            this.m_comportementBalle.changeDirectionBalleAvecRaquetteOrdinateur();

            // On change la vitesse de la balle à cause de l'impact.
            this.m_comportementBalle.ajoutVitesseBalleImpact();
        }

        // Test s'il y a une collision avec la raquette de la joueuse.
        if (this.m_comportementRaquetteJoueuse.testCollisionAvecBalle() == true) {
            // On change les directions à cause de l'impact de la raquette.
            this.m_comportementBalle.changeDirectionBalleAvecRaquetteJoueuse();

            // On change la vitesse de la balle à cause de l'impact.
            this.m_comportementBalle.ajoutVitesseBalleImpact();
        }
    }

    /**
     * Commencer  l'échange.
     */
    commencerEchange() {
        // Comportement de la balle.
        this.m_comportementBalle.commencerEchange();

        // Comportement de la joueuse.
        this.m_comportementRaquetteJoueuse.commencerEchange();

        // Comportement ordinateur.
        this.m_comportementRaquetteOrdinateur.commencerEchange();
    }

    /**
     * L'ordinateur gagne.
     */
    ordinateurGagneEchange() {
        // On recommence l'échange.
        this.commencerEchange();
    }

    /**
     * La joueuse gagne l'échange.
     */
    joueuseGagneEchange() {
        // On recommence l'échange.
        this.commencerEchange();
    }

    /**
     * Permet de dessiner les objets.
     */
    dessiner() {
        // On commence par dessiner le terrain.
        this.m_terrain.dessiner(this.m_crayon);

        // On dessine la balle.
        this.m_balle.dessiner(this.m_crayon);

        // On dessine la raquette de la joueuse.
        this.m_raquetteJoueuse.dessiner(this.m_crayon);

        // On dessine la raquette de l'ordinateur.
        this.m_raquetteOrdinateur.dessiner(this.m_crayon);
    }

    /**
     * Permet d'écrire le score.
     */
    ecrireScore() {
        // Score de l'odinateur sur la partie haute du terrain.
        this.m_crayon.ecrireText(this.m_largeurTerrain / 2, this.m_hauteurTerrain / 4, this.m_scoreOrdinateur, "#454544");

        // Score de la joueuse sur la partie basse du terrain.
        this.m_crayon.ecrireText(this.m_largeurTerrain / 2, this.m_hauteurTerrain - this.m_hauteurTerrain / 4, this.m_scoreJoueuse, "#454544");
    }
}
