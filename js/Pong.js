// Le canvas est un élément HTML permettant de dessiner dans une page web. Grâce à son contexte, on va dessiner le terrain, raquette etc.
let canvas = document.getElementById("canvas");

// Creation du moteur.
let moteur = new Moteur(canvas);

// Chaque jeu vidéo à une boucle. C'est dans cette boucle que les interactions et le dessin sont réalisés.
// En effet à chaque tour de la boucle nous allons calculer les différentes opérations comme le déplacement
// de la raquette puis dessiner l'image. C'est la succession d'images qui va donner l'impression de temps réel.
// Cette boucle correspond au taux de rafraichissement du jeu, on parle de fps (frame per seconds) ou images par secondes.
// Lorsque un jeu rame, il y a tout simplement trop de calcul fait dans la boucle et on distingue la succession d'image, ce n'est plus assez fluide.
function animer() {
    // Appel d'une fonction permettant au navigateur de re-demander l'appel de la fonction animer à son prochain rafraichissement.
    requestAnimationFrame(animer);

    // Rafraichir.
    // Ici nous demandons au moteur de rafraichir les données grâce au tableau de touches enfoncée.
    moteur.rafraichir();

    // Dessiner.
    // Ici nous demandons au moteur de dessiner les différents objets dans le canvas.
    moteur.dessiner();
}

// Premier appel de animer qui va s'éxécuter au lancement de la page web.
animer();
