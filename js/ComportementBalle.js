/**
 * Permet de définir le comportement de la balle.
 */
class ComportementBalle {
    /**
     * Constructeur.
     * @param {Balle} p_balle est la balle.
     * @param {Raquette} p_raquetteJoueuse est la raquette de la joueuse.
     * @param {Raquette} p_raquetteOrdinateur est la raquette de l'ordinateur.
     * @param {Terrain} p_terrain est le terrain.
     */
    constructor(p_balle, p_raquetteJoueuse, p_raquetteOrdinateur, p_terrain) {
        // On sauve la balle dans l'objet.
        this.m_balle = p_balle;
        // On sauve la raquette de la joueuse.
        this.m_raquetteJoueuse = p_raquetteJoueuse;
        // On sauve la raquette de l'ordinateur.
        this.m_raquetteOrdinateur = p_raquetteOrdinateur;
        // On sauve le terrain.
        this.m_terrain = p_terrain;

        // On définit la vitesse ajoutée à chaque impact de la raquette.
        this.m_vitesseSupplementaireImpact = 0.25;

        // On définit la vitesse maximum de la balle.
        this.m_vitesseBalleMaximum = 4;

        // On définit la vitesse minimum de la balle.
        this.m_vitesseBalleMinimum = 2;

        // On définit la vitesse de la balle par défault.
        this.m_vitesseBalle = this.m_vitesseBalleMinimum;

        // On définit la direction en x de la balle.
        this.m_directionBalleX = 0;

        // On définit  la direction en y de la balle.
        this.m_directionBalleY = 1;
    }

    /**
     * Commencer l'échange.
     */
    commencerEchange() {
        // TODO : Quel est l'état du jeu (position, direction et vitesse de la balle) quand on commence un échange ?
    }

    /**
     * Permet de rafraîchir l'affichage de la balle.
     */
    rafraichirBalle() {
        // Valeurs courantes définissant la position de la balle.
        let rayon = this.m_balle.getRayon();
        let positionX = this.m_balle.getX();
        let positionY = this.m_balle.getY();

        // Mise à jour de la position.
        positionX = positionX + this.m_directionBalleX * this.m_vitesseBalle;
        positionY = positionY + this.m_directionBalleY * this.m_vitesseBalle;

        // Mise à jour de la direction de la balle.
        // Test avec le bord gauche du terrain pour que la balle reste dans le terrain.
        if (positionX < rayon) {
            // Change la direction en x pour repartir dans l'autre sens.
            this.m_directionBalleX = -this.m_directionBalleX;
        }

        // Test avec le bord droit du terrain pour que la balle reste dans le terrain.
        if (positionX > this.m_terrain.getLargeur() - rayon) {
            // Change la direction en x pour repartir dans l'autre sens.
            this.m_directionBalleX = -this.m_directionBalleX;
        }

        // Rafraîchir la balle avec la nouvelle position.
        this.m_balle.rafraichirPosition(positionX, positionY);
    }

    /**
     * Permet d'ajouter de la vitesse à la balle lors de l'impact.
     */
    ajoutVitesseBalleImpact() {
        // TODO : Comment peut-on ajouter de la vitesse à la balle ?
        // TODO : Est-ce qu'il y a des contraintes que l'on doit vérifier ?
    }

    /**
     * Permet de changer la direction de la balle lors de la collision avec la raquette d'ordinateur.
     */
    changeDirectionBalleAvecRaquetteOrdinateur() {
        // Récupération de la position X de la balle.
        let positionBalleX = this.m_balle.getX();

        // On divise la raquette en 5 parties.
        let largeurSegment = this.m_raquetteOrdinateur.getLargeur() / 5;
        let raquetteCentre = this.m_raquetteOrdinateur.getX();
        let raquetteMilieuDroite = raquetteCentre + largeurSegment / 2;
        let raquetteExtremiteDroite = raquetteMilieuDroite + largeurSegment;
        let raquetteMilieuGauche = raquetteCentre - largeurSegment / 2;
        let raquetteExtremiteGauche = raquetteMilieuGauche - largeurSegment;

        // Test de la partie centre.
        if (positionBalleX >= raquetteMilieuGauche && positionBalleX <= raquetteMilieuDroite) {
            // La balle part à la verticale.
            this.m_directionBalleX = 0;
            this.m_directionBalleY = 1;
        }
        // Test la partie centre droit.
        else if (positionBalleX > raquetteMilieuDroite && positionBalleX <= raquetteExtremiteDroite) {
            // La balle se décale un peu sur x (à droite).
            this.m_directionBalleX = 1;
            this.m_directionBalleY = 1;
        }
        // Test partie droite.
        else if (positionBalleX > raquetteExtremiteDroite) {
            // La balle se décale beaucoup sur x (à droite).
            this.m_directionBalleX = 3;
            this.m_directionBalleY = 1;
        }
        // Test la partie centre gauche.
        else if (positionBalleX < raquetteMilieuGauche && positionBalleX >= raquetteExtremiteGauche) {
            // La balle se décale un peu sur x (à gauche).
            this.m_directionBalleX = -1;
            this.m_directionBalleY = 1;
        }
        // Test partie droite.
        else if (positionBalleX < raquetteExtremiteGauche) {
            // La balle se décale beaucoup sur x (à gauche).
            this.m_directionBalleX = -3;
            this.m_directionBalleY = 1;
        }
    }

    /**
     * Permet de changer la direction de la balle lors de la collision avec la raquette de la joueuse.
     */
    changeDirectionBalleAvecRaquetteJoueuse() {
        // TODO : On divise la raquette en 5 parties: extrémité gauche, milieu gauche, milieu, milieu droite, extrémité droite
        // TODO : la direction va être différentee selon la partie de la raquette qui est touchée par la balle.
        // TODO : Définir la nouvelle direction.
    }
}