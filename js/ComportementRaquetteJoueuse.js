/**
 * Permet de définir le comportement de la joueuse.
 */
class ComportementRaquetteJoueuse {
    /**
     * Constructeurr.
     * @param {Raquette} p_raquette est la raquette de la joueuse.
     * @param {Terrain} p_terrain est le terrain.
     * @param {Balle} p_balle est la balle.
     * @param {Clavier} p_clavier est le clavier.
     */
    constructor(p_raquette, p_terrain, p_balle, p_clavier) {
        // On sauve la raquette localement.
        this.m_raquette = p_raquette;
        // On sauve le terrain.
        this.m_terrain = p_terrain;
        // On sauve la balle.
        this.m_balle = p_balle;
        // On sauve le clavier.
        this.m_clavier = p_clavier;

        // Définition de  la vitesse de la raquette de l'ordinateur.
        this.m_vitesseRaquette = 3;
    }

    /**
     * Test si la joueuse rate la balle.
     * @returns {boolean} Retourne true si la joueuse rate la balle, sinon false.
     */
    rateBalle() {
        // Si la balle est en collision avec la raquette, alors la joueuse ne rate pas la balle.
        if (this.testCollisionAvecBalle() == true) {
            return false;
        }

        // TODO : Dans quelle situation est-on toujours certain que la balle a été ratée par la joueuse ?
        // TODO : Ajouter le test le vérifiant.

        // Dans tous les autres cas on considère que l'ordinateur n'a pas raté la balle :
        // elle est forcément dans le terrain car elle ne peut pas sortir sur les côtés.
        return false;
    }

    /**
     * Test s'il y une collision entre la balle et la raquette de la joueuse.
     * @returns {boolean}  Retourne true s'il y a une collision, false sinon.
     */
    testCollisionAvecBalle() {
        // TODO: Il est plus simple de trouver quand n'y a pas de collision entre la raquette et la balle.
        // TODO : il est existe plusieurs conditions permettant de le vérifier : les identifier et les coder.

        // On a pas trouvé le fait qu'il n'y ait pas de collision, il y a donc une collision.
        return true;
    }

    /**
     * Commencer échange.
     */
    commencerEchange() {
        // TODO Dans quel état (position) doit être la joueuse (donc sa raquette) au début d'un échange ?
    }

    /**
     * Permet de rafraîchir la raquette de la joueuse.
     */
    rafraichirRaquetteJoueuse() {
        // TODO : il faut rafraichir la position de la raquette de la joueuse. Cette position évolue en fonction
        // TODO : des touches pressées par la joueuse. Il faut aussi prendre en compte la vitesse de la raquette.
        // TODO : Attention: la raquette ne doit pas sortir du terrain !
    }
}