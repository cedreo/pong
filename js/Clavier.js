// Variable pour stocker la touche enfoncée.
let toucheEnfonce = {};

/**
 * Objet permettant de gérer le clavier.
 */
class Clavier {
    /**
     * Constructeur.
     */
    constructor() {
    }

    /**
     * Permet d'ajouter l'écoute clavier.
     */
    ajouterEcouteClavier() {
        // Permet d'ajouter un écouteur sur les évènement liés au clavier.
        window.addEventListener("keydown", function (event) {
            toucheEnfonce[event.keyCode] = true;
        });

        window.addEventListener("keyup", function (event) {
            delete toucheEnfonce[event.keyCode];
        });
    }

    /**
     * Indique si la touche gauche du clavier est enfoncée.
     * @returns Retourne true si la touche gauche est enfoncée, false sinon.
     */
    toucheGaucheEnfonce() {
        if (toucheEnfonce['37']) {
            return true;
        }

        return false;
    }

    /**
     * Indique si la touche droite du clavier est enfoncée.
     * @returns Retourne true si la touche droite est enfoncée, false sinon.
     */
    toucheDroiteEnfonce() {
        if (toucheEnfonce['39']) {
            return true;
        }

        return false;
    }
}