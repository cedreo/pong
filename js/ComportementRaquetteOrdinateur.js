/**
 * Permet de définir le comportement de l'ordinateur.
 */
class ComportementRaquetteOrdinateur {
    /**
     * Constructeur.
     * @param {Raquette} p_raquette est la raquette de l'ordinateur.
     * @param {Terrain} p_terrain est le terrain.
     * @param {Balle} p_balle est la balle.
     */
    constructor(p_raquette, p_terrain, p_balle) {
        // On sauve la raquette localement.
        this.m_raquette = p_raquette;
        // On sauve le terrain.
        this.m_terrain = p_terrain;
        // On sauve la balle.
        this.m_balle = p_balle;

        // Définition de la vitesse de la raquette de l'odinateur.
        this.m_vitesseRaquette = 3;
    }

    /**
     * Commencer échange.
     */
    commencerEchange() {
        // TODO : À quel endroit doit être la raquette de l'ordinateur au début de l'échange ?
    }

    /**
     * Test si l'ordinateur rate la balle.
     * @returns {boolean} Retourne true si l'ordinateur rate la balle, sinon false.
     */
    rateBalle() {
        // Si la balle est en collision avec la raquette, alors l'ordinateur ne rate pas la balle.
        if (this.testCollisionAvecBalle() == true) {
            return false;
        }

        // Si la balle est sortie du terrain (vers le haut), alors l'ordinateur a raté la balle.
        if (this.m_balle.getY() < 0) {
            return true;
        }

        // Dans tous les autres cas on considère que l'ordinateur n'a pas raté la balle :
        // elle est forcément dans le terrain car elle ne peut pas sortir sur les côtés.
        return false;
    }

    /**
     * Test s'il y une collision entre la balle et la raquette de l'odinateur.
     * @returns {boolean} Retourne true s'il y a une collision, false sinon.
     */
    testCollisionAvecBalle() {
        // On va chercher à trouver qu'il n'y a pas de collision.

        // Test avec la positon en y.
        if (this.m_balle.getY() - this.m_balle.getRayon() > this.m_raquette.getY() + this.m_raquette.getHauteur() / 2) {
            return false;
        }

        // Test avec le coté gauche de la raquette.
        if (this.m_balle.getX() + this.m_balle.getRayon() < this.m_raquette.getX() - this.m_raquette.getLargeur() / 2) {
            return false;
        }

        // Test avec le coté droit de la raquette.
        if (this.m_balle.getX() - this.m_balle.getRayon() > this.m_raquette.getX() + this.m_raquette.getLargeur() / 2) {
            return false;
        }

        // On a pas trouvé le fait qu'il n'y ait pas de collision, il y a donc une collision.
        return true;
    }

    /**
     * Permet de rafraîchir la raquette de l'ordinateur.
     */
    rafraichirRaquetteOrdinateur() {
        // TODO : Il faut déterminer la position de la raquette de l'ordinateur. Cela doit se faire en fonction de la position de la balle.
        // TODO : Mais attention à ne pas le rendre invicible : cela ne serait pas drôle pour la joueuse !
        // TODO : Il faut également faire attention à ce que la raquette ne sorte pas du terrain.
    }
}
