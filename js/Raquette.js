/**
 * Objet permettant de definir une raquette.
 */
class Raquette {
    /**
     * Constructeur.
     */
    constructor() {
        // Une raquette est définie par une position x et y.
        this.m_x = 0;
        this.m_y = 0;
        // Et également définie par ses dimensions : largeur et hauteur.
        this.m_largeur = 50;
        this.m_hauteur = 5;
    }

    /**
     * Permet de rafraîchir la position de la balle.
     * @param p_x est la nouvelle position sur l'axe horizontal.
     * @param p_y est la nouvelle position sur l'axe vertical.
     */
    rafraichirPosition(p_x, p_y) {
        this.m_x = p_x;
        this.m_y = p_y;
    }

    /**
     * Permet de dessiner la raquette.
     * @param {Crayon} p_crayon est le crayon permettant de dessiner.
     */
    dessiner(p_crayon) {
        // TODO : La raquette est un rectangle : utiliser le crayon pour le dessiner.
    }

    /**
     * Retourne la position en x.
     * @returns Retourne la position en x.
     */
    getX() {
        return this.m_x;
    }

    /**
     * Retourne la position en y.
     * @returns Retourne la position en y.
     */
    getY() {
        return this.m_y;
    }

    /**
     * Retourne la largeur.
     * @returns Retourne la largeur.
     */
    getLargeur() {
        return this.m_largeur;
    }

    /**
     * Retourne la hauteur.
     * @returns Retourne la hauteur.
     */
    getHauteur() {
        return this.m_hauteur;
    }
}