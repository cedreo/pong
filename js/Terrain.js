/**
 * Objet permettant de définir un terrain.
 */
class Terrain {
    /**
     * Constructeur.
     * @param p_largeur est la largeur du terrain.
     * @param p_hauteur est la hauteur du terrain.
     */
    constructor(p_largeur, p_hauteur) {
        this.m_largeur = p_largeur;
        this.m_hauteur = p_hauteur;
    }

    /**
     * Permet de dessiner le terrain.
     * @param {Crayon} p_crayon est le crayon permettant de dessiner.
     */
    dessiner(p_crayon) {
        // On dessine le terrain.
        p_crayon.dessinerRectange(0, 0, this.m_largeur, this.m_hauteur, "#04afa7");

        // On dessine le filet.
        // TODO : Le filet est un rectangle à placer au milieu du terrain.
    }

    /**
     * Retourne la largeur du terrain.
     * @returns Retourne la largeur du terrain.
     */
    getLargeur() {
        return this.m_largeur;
    }

    /**
     * Retourne la hauteur du terrain.
     * @returns Retourne la hauteur du terrain.
     */
    getHauteur() {
        return this.m_hauteur;
    }
}