/**
 * Permet de dessiner les différents objets selon une position sur l'axe X et l'axe Y.
 * Le 0 est en haut à gauche pour les deux axes.
 *  0--------------> X
 *  |
 *  |
 *  |
 *  |
 *  V
 *  Y
 *
 */
class Crayon {
    /**
     * Constructeur de l'objet.
     * @param p_context2d est le contexte permettant de dessiner.
     */
    constructor(p_context2d) {
        this.context2d = p_context2d;
    }

    /**
     * Permet de dessiner un rectangle.
     * @param p_x est la valeur sur l'axe horizontal.
     * @param p_y est la valeur sur l'axe vertical.
     * @param p_largeur est la largeur du rectangle.
     * @param p_hauteur est la hauteur du rectangle.
     * @param p_couleur est la couleur souhaitée.
     */
    dessinerRectange(p_x, p_y, p_largeur, p_hauteur, p_couleur) {
        this.context2d.fillStyle = p_couleur;
        this.context2d.fillRect(p_x, p_y, p_largeur, p_hauteur);
    }

    /**
     * Permet de dessiner un rond.
     * @param p_x est la valeur sur l'axe horizontal.
     * @param p_y est la valeur sur l'axe vertical.
     * @param p_rayon est le rayon du rond.
     * @param p_couleur est la couleur souhaitée.
     */
    dessinerRond(p_x, p_y, p_rayon, p_couleur) {
        this.context2d.beginPath();
        this.context2d.arc(p_x, p_y, p_rayon, 0, 2 * Math.PI, false);
        this.context2d.fillStyle = p_couleur;
        this.context2d.fill();
    }

    /**
     * Permet d'écrire le text.
     * @param p_x est la valeur sur l'axe horizontal.
     * @param p_y est la valeur sur l'axe vertical.
     * @param p_text est le texte a écrire.
     * @param p_couleur est la couleur souhaitée.
     */
    ecrireText(p_x, p_y, p_text, p_couleur) {
        this.context2d.font = "30px Arial";
        this.context2d.textAlign = "center";
        this.context2d.fillStyle = p_couleur;
        this.context2d.fillText(p_text, p_x, p_y);
    }
}
