# Projet CEDREO Pong

Ce projet est un exercice pour appréhender les concepts d'un jeu en 2D dans un navigateur Web. Nous l'utilisons comme outil pédagogique lorsque nous recevons des stagiaires novices en développement.

![Illustration](doc/screenshot.png)

Le master contient la trame vide (avec des TODO à remplir). La solution est disponible dans la branche du même nom.

Il existe deux présentations situées dans le répertoire doc: 
 - [CedreoPong.pdf](doc/CedreoPong.pdf): qui décrit les concepts et les choses à réaliser.
 - [CedreoPongAnimation.pdf](doc/CedreoPongAnimation.pdf): qui permet d'expliquer à la manière d'un flipbook ce qu'est l'animation 2D. 